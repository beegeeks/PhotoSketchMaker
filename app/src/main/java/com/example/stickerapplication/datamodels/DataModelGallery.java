package com.beegeeks.backgrounds.wallpapers.photextgallery;

public class WallsDM {
    private String imagePath;

    public WallsDM(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }
}
