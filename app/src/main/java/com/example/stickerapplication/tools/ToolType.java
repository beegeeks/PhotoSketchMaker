package com.example.stickerapplication.tools;

/**
 * @author <a href="https://github.com/burhanrashid52">Burhanuddin Rashid</a>
 * @version 0.1.2
 * @since 5/23/2018
 */
public enum ToolType {
    FILTERS,
    BRUSH,
    ERASER,
    TEXT,
    EMOJI
}
